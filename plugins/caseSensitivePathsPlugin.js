/* eslint-disable strict */

'use strict';

const path = require('path');

function CaseSensitivePathsPlugin(options) {
  this.options = options || {};
  this.reset();
}

CaseSensitivePathsPlugin.prototype.reset = function () {
  this.pathCache = {};
  this.fsOperations = 0;
  this.primed = false;
};

CaseSensitivePathsPlugin.prototype.getFilenamesInDir = function (dir, callback) {
  const that = this;
  const fs = this.compiler.inputFileSystem;
  this.fsOperations += 1;

  if (Object.prototype.hasOwnProperty.call(this.pathCache, dir)) {
    callback(this.pathCache[dir]);
    return;
  }
  if (this.options.debug) {
    console.log('[CaseSensitivePathsPlugin] Reading directory', dir);
  }

  fs.readdir(dir, (err, files) => {
    if (err) {
      if (that.options.debug) {
        console.log('[CaseSensitivePathsPlugin] Failed to read directory', dir, err);
      }
      callback([]);
      return;
    }

    callback(files.map(f => f.normalize ? f.normalize('NFC') : f));
  });
};

CaseSensitivePathsPlugin.prototype.fileExistsWithCase = function (filepath, callback) {
  // Split filepath into current filename (or directory name) and parent directory tree.
  const that = this;
  const dir = path.dirname(filepath);
  const filename = path.basename(filepath);
  const parsedPath = path.parse(dir);

  // If we are at the root, or have found a path we already know is good, return.
  if (parsedPath.dir === parsedPath.root || dir === '.' || Object.prototype.hasOwnProperty.call(that.pathCache, filepath)) {
    callback();
    return;
  }

  // Check all filenames in the current dir against current filename to ensure one of them matches.
  // Read from the cache if available, from FS if not.
  that.getFilenamesInDir(dir, (filenames) => {
    // If the exact match does not exist, attempt to find the correct filename.
    if (filenames.indexOf(filename) === -1) {
      // Fallback value which triggers us to abort.
      let correctFilename = '!nonexistent';

      for (let i = 0; i < filenames.length; i += 1) {
        if (filenames[i].toLowerCase() === filename.toLowerCase()) {
          correctFilename = `\`${filenames[i]}\`.`;
          break;
        }
      }
      callback(correctFilename);
      return;
    }

    // If exact match exists, recurse through directory tree until root.
    that.fileExistsWithCase(dir, (recurse) => {
      // If found an error elsewhere, return that correct filename
      // Don't bother caching - we're about to error out anyway.
      if (!recurse) {
        that.pathCache[dir] = filenames;
      }

      callback(recurse);
    });
  });
};

CaseSensitivePathsPlugin.prototype.primeCache = function (callback) {
  if (this.primed) {
    callback();
    return;
  }

  const that = this;
  // Prime the cache with the current directory. We have to assume the current casing is correct,
  // as in certain circumstances people can switch into an incorrectly-cased directory.
  const currentPath = path.resolve();
  that.getFilenamesInDir(currentPath, (files) => {
    that.pathCache[currentPath] = files;
    that.primed = true;
    callback();
  });
};

CaseSensitivePathsPlugin.prototype.apply = function (compiler) {
  this.compiler = compiler;

  const onDone = () => {
    if (this.options.debug) {
      console.log('[CaseSensitivePathsPlugin] Total filesystem reads:', this.fsOperations);
    }

    this.reset();
  };

  const onAfterResolve = (data, done) => {
    this.primeCache(() => {
      // Trim ? off, since some loaders add that to the resource they're attemping to load
      let pathName = data.resource.split('?')[0];
      pathName = pathName.normalize ? pathName.normalize('NFC') : pathName;

      this.fileExistsWithCase(pathName, (realName) => {
        if (realName) {
          if (realName === '!nonexistent') {
            // If file does not exist, let Webpack show a more appropriate error.
            done(null, data);
          } else {
            done(new Error(`[CaseSensitivePathsPlugin] \`${pathName}\` does not match the corresponding path on disk ${realName}`));
          }
        } else {
          done(null, data);
        }
      });
    });
  };

  if (compiler.hooks) {
    compiler.hooks.done.tap('CaseSensitivePathsPlugin', onDone);
    compiler.hooks.normalModuleFactory.tap('CaseSensitivePathsPlugin', (nmf) => {
      nmf.hooks.afterResolve.tapAsync('CaseSensitivePathsPlugin', onAfterResolve);
    });
  } else {
    compiler.plugin('done', onDone);
    compiler.plugin('normal-module-factory', (nmf) => {
      nmf.plugin('after-resolve', onAfterResolve);
    });
  }
};

module.exports = CaseSensitivePathsPlugin;
