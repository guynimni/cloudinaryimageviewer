const webpack = require('webpack');
const webpackConfig = require('../../webpack.config.prod');

process.traceDeprecation = true;

const start = new Date();

webpack(webpackConfig).run((err, stats) => {
  
  if (err) {
    process.stdout.write(err + "\n");
    return 1;
  }

  const jsonStats = stats.toJson();

  if (jsonStats.hasErrors) {
    return jsonStats.errors.map(error => process.stdout.write(error + "\n"));
  }

  if (jsonStats.hasWarnings) {
    process.stdout.write("Warnings:\n");
    jsonStats.warnings.map(warning => process.stdout.write(warning + "\n"));
  }

  let elapsed = (new Date()).getTime() - start.getTime();
  const minutes = Math.floor(elapsed/(1000*60));
  elapsed -= minutes*(1000*60);
  const seconds = Math.floor(elapsed / 1000);
  const elapsedMessage = minutes > 0 ? `${minutes < 10 ? `0${minutes}`: minutes}:${seconds < 10 ? `0${seconds}`: seconds} minutes` : `${seconds < 10 ? `0${seconds}`: seconds} seconds`;
  process.stdout.write(`\x1b[7mBuild completed. Time: ${elapsedMessage}\x1b[0m\n`);
  
  return 0;

});
