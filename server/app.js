const express = require('express');
const path = require('path');

const app = express();
const siteRouter = express.Router();
app.disable('etag');

process.stdout.write(`Starting ${process.env.NODE_ENV} environment\n`);

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const config = require('../webpack.config');

  const compiler = webpack(config);

  const DevWebpackMiddleware = require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  });

  const HotWebpackMiddleware = require('webpack-hot-middleware')(compiler);

  app.use(DevWebpackMiddleware);
  app.use(HotWebpackMiddleware);
  siteRouter.get('*', function(req, res) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.sendFile(path.join( __dirname, '..', 'client', 'index.html'));
  });
}
else {
  app.use(express.static("dist"));
}

app.use('/favicon.ico', express.static('favicon.ico'));
app.use('/', siteRouter);

module.exports = app;
