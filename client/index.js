import React from 'react';
import ReactDOM from 'react-dom';
require('jquery/dist/jquery.min');
window.jQuery = $;
import 'bootstrap/dist/js/bootstrap.bundle.min';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, BrowserRouter, Switch, Redirect} from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import 'Styles/main.scss';

import MasterPage from 'Pages/_MasterPage';
import ImageViewPage from 'Pages/ImageViewPage';
import AboutPageView from 'Pages/AboutViewPage';

ReactDOM.render(
  <BrowserRouter>
      <Switch>
        <Route exact path={"/"} render={() => (
          <MasterPage><ImageViewPage /></MasterPage>
        )} />
        <Route exact path={"/about"} render={() => (
          <MasterPage><AboutPageView /></MasterPage>
        )} />
        <Route render={function(){return (<Redirect to="/" />);}} />
      </Switch>
    </BrowserRouter>,
  document.getElementById('react')
);
