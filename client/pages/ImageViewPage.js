import React, { Component, Fragment } from "react";
import ImagesGrid from "Components/ImagesGrid";
import ImageController from "Controllers/ImageController";
import Spinner from "Components/Spinner";

class ImageViewPage extends Component {
  constructor (props) {
    super(props);
    this.state = {
      searchValue: "",
      images: null
    }
    this.handleSearchClick = this.handleSearchClick.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleKeypress = this.handleKeypress.bind(this);
    this.onImagesChanged = this.onImagesChanged.bind(this);
  }
  
  componentDidMount() {
    this.subscriptionKey = ImageController.subscribeToImagesChange(this.onImagesChanged);
  }
  
  componentWillUnmount() {
    ImageController.unsubscribeFromImagesChange(this.subscriptionKey);
  }
  
  onImagesChanged(images) {
    this.setState({images});
  }
  
  handleSearchClick() {
    this.setState({images: [], loading: this.state.searchValue ? true : false});
    
    if (this.state.searchValue)
      ImageController.getImages(this.state.searchValue).then(images => this.setState({loading: false}));
  }
  
  handleSearchChange(event) {
    this.setState({searchValue: event.target.value});
  }
  
  handleKeypress(event) {
    if (event.nativeEvent.keyCode === 13)
      this.handleSearchClick();
  }
  
  getImagesGrid() {
    if (this.state.loading)
      return (<Spinner />);
    
    if (!this.state.images)
      return null;
    else 
      return <ImagesGrid images={this.state.images}/>
  }
  
  render () {
    return (
      <Fragment>
        <div className="row">
          <div className="col-lg-12 text-center">
            <h1 className="mt-5">Type the first word that comes to mind</h1>
            <p className="lead">We will do our best to find the right images for you!</p>
          </div>
        </div>
        <div className="row d-flex justify-content-center">
          <div className="col-md-12 col-lg-6 text-center">
            <div className="input-group mb-3">
              <input type="text" 
                     className="form-control" 
                     placeholder="Try 'child' for example" 
                     aria-label="Search phrase" 
                     aria-describedby="basic-addon2"  
                     onKeyPress={this.handleKeypress} 
                     value={this.state.searchValue} 
                     onChange={this.handleSearchChange}/>
                <div className="input-group-append">
                  <button className="btn btn-outline-secondary" type="button" onClick={this.handleSearchClick}>Let's go</button>
                </div>
            </div>
          </div>
        </div>
        <div className="row d-flex justify-content-center">
          {this.getImagesGrid()}
        </div>
      </Fragment>
      
    );
  }
}

export default ImageViewPage;
