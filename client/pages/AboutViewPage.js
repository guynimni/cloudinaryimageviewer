import React, { Component } from "react";

class ImageViewPage extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div className="row">
        <div className="col-lg-12 text-center">
          <h1 className="mt-5">Developed with joy :)</h1>
          <p className="lead">Just a simple POC - TBE (to be expended :))</p>
        </div>
      </div>
    );
  }
}

export default ImageViewPage;
