import BaseImage from 'Entities/images/BaseImage';

class BaseImageAPI {
  constructor() {
    this.baseUrl = "";
    this.page = 1;
    this.totalPerPage = 20;
  }
  
  getImages(searchString) {
    return Promise.resolve([]);
  }
  
  getURL(searchString) {
    return this.baseUrl;
  }
  
  generateImage(id, highResURL, width, height, thumbnailURL, thumbnailHeight, thumbnailWidth, name) {
    return new BaseImage({id, highResURL, width, height, thumbnailURL, thumbnailHeight, thumbnailWidth, name});
  }
  
}

export default BaseImageAPI;
