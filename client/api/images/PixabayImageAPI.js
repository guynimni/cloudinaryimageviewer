import BaseImageAPI from 'API/images/BaseImageAPI';
import AjaxUtils from 'Utils/AjaxUtils';
import BaseImage from "Entities/images/BaseImage";

class PixabayImageAPI extends BaseImageAPI {
  constructor () {
    super();
    this.baseUrl = "https://pixabay.com/api/?key=13184225-4f800930d295d925a896495ef&image_type=photo";
  }
  
  getImages(searchString) {
    const URL = this.getURL(searchString);
    return AjaxUtils.get(URL).then(data => {
      const dataArray = (data && data.hits) || [];
      const images = [];
      
      for (let i = 0; i < dataArray.length; i++) {
        images.push(this.imageFromServerData(dataArray[i]));
      }
      
      return images;
    });
  }
  
  imageFromServerData(serverData) {
    const nameRegex = /(https:\/\/pixabay.com\/photos\/)([^/]*)(.*)/;
    const image = this.generateImage(
      serverData.largeImageURL,
      serverData.largeImageURL,
      serverData.imageWidth,
      serverData.imageHeight,
      serverData.webformatURL,
      serverData.webformatHeight,
      serverData.webformatWidth,
      serverData.pageURL.replace(nameRegex, '$2')
    )
    
    return image;
  }
  
  getURL(searchString) {
    return `${this.baseUrl}&q=${searchString}&page=${this.page}&per_page=${this.totalPerPage}`;
  }
}

export default PixabayImageAPI;
