import PixabayImageAPI from 'API/images/PixabayImageAPI';

let instance = null;

class ImageController {
  constructor() {
		if(!instance) {
			instance = this;
		}
		this.imagesById = {}; // We store the images in an object for faster retrieval
		this.subscribers = {};
		return instance;
	}
	
  getImages(searchPhrase) {
    const imageAPIs = [new PixabayImageAPI()]; // Ready to expand to more image apis
    const imagePromises = [];
    this.imagesById = {};
      
    for (let i = 0; i < imageAPIs.length; i++) {
      imagePromises.push(imageAPIs[i].getImages(searchPhrase));
    }
    
    return  Promise.all(imagePromises)
            .then(arr => arr.flat())
            .then(imagesArray => {
              for (let i = 0; i < imagesArray.length; i++)
                this.imagesById[imagesArray[i].id] = imagesArray[i];
              
              this._informSubscribers();
              
              return imagesArray;
            });
  }
  
  updateImage(image) {
    this.imagesById[image.id] = image;
    this._informSubscribers();
  }
  
  deleteImage(image) {
    delete this.imagesById[image.id];
    this._informSubscribers();
  }
  
  subscribeToImagesChange(callback) {
    const key = Object.keys(this.subscribers).length;
    this.subscribers[key] = callback;
    return key;
  }
  
  unsubscribeFromImagesChange(key) {
    delete this.subscribers[key];
  }
  
  _informSubscribers() {
    for (let subscriberKey in this.subscribers) {
      this.subscribers[subscriberKey](Object.values(this.imagesById));
    }
  }
}

export default new ImageController();
