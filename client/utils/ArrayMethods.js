export default class ArrayMethods {
  static spreadArrayToColumns(array, chunks) {
    // For example:
    // Input => array: [1,2,3,4,5,6,7,8,9,10,11,12], chunks: 4
    // Output: [[1,5,9], [2,6,10], [3,7,11], [4,8,12]]
    const arrayOfArrays = [];
    for (let i = 0; i < array.length; i+=chunks) {
      for (let k = 0; k < chunks; k++) {
        if (!arrayOfArrays[k]) {
          arrayOfArrays[k] = []
        }
        
        arrayOfArrays[k].push(array[i+k])
      }
    }
    return arrayOfArrays;
  }
}
