import PropTypes from "prop-types";

export default Object.assign({}, PropTypes, {
  arrayOfInstances: (desiredClass) => {
    return (props, propName, componentName) => {
      const propValue = props[propName];
      if (!Array.isArray(propValue))
        return new Error(`${componentName} prop ${propName} should be an array`);
      for (let i = 0; i < propValue.length; i++) {
        if (!(propValue[i] instanceof desiredClass))
          return new Error(`${componentName} prop ${propName} should be an array of instances of ${desiredClass.name}`);
      }
    };
  }
});

