class AjaxUtils {
  
  static get(url, headersExtend = {}, requestType = null, requestUID = null) {
    let initData = {
      method: 'GET',
      headers: headersExtend,
      cache: 'default'
    };
    return fetch(url).then(data => data.json());
  }
  
}

export default AjaxUtils;
