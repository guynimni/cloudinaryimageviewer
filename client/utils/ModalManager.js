import React from 'react';
import ReactDOM from 'react-dom';

let instance = null;

class ModalManager {
  constructor() {
		if(!instance) {
			instance = this;
		}
		this.modalsElement = document.getElementById('modals');
		return instance;
	}
	
  renderModal(Component, componentProps) {
    const newModalContainer = document.createElement("div");
    this.modalsElement.appendChild(newModalContainer);
    
    const ModalContent = <Component {...componentProps} onClose={this.destroyModal.bind(this, newModalContainer)} />
    ReactDOM.render(ModalContent, newModalContainer);
  }
  
  destroyModal(modalContainerElement) {
    ReactDOM.unmountComponentAtNode(modalContainerElement);
    this.modalsElement.removeChild(modalContainerElement);
  }
}

export default new ModalManager();
