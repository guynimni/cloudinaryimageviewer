import React, { Fragment, useState } from 'react';
import BaseImage from 'Entities/images/BaseImage';
import PropTypes from 'Utils/propTypes';
import 'Styles/components/imageThumbnail.scss';
import ModalManager from 'Utils/ModalManager';
import ImageModal from "Components/ImageModal";

const ImageThumbnail = (props) => {
  
  function handleClick() {
    ModalManager.renderModal(ImageModal, {image: props.image});
  }
  
  if (!props.image)
    return null;
  else
    return (
      <Fragment>
        <a data-ctarget={`#${btoa(props.image.highResURL)}`} data-ctoggle="modal" href="#" onClick={handleClick}>
          <div className="image-thumbnail-container">
              <img src={props.image.thumbnailURL} />
            <div className="overlay-text">{props.image.name}</div>
          </div>
        </a>
      </Fragment>
    );
};

ImageThumbnail.propTypes = {
  image: PropTypes.instanceOf(BaseImage)
};

export default ImageThumbnail;
