import React from "react";
import PropTypes from "prop-types";
import BaseImage from "Entities/images/BaseImage";
import 'Styles/components/imageModal.scss';
import ImageController from 'Controllers/ImageController';

class ImageModal extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      show: true,
      imageName: this.props.image.name || ""
    }
    
    this.cancel = this.cancel.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
  }
  
  hide(show) {
    this.setState({show});
    if (this.props.onClose)
      this.props.onClose();
  }
  
  handleNameChange(event) {
    this.setState({imageName: event.target.value});
  }
  
  delete() {
    ImageController.deleteImage(this.props.image);
    this.hide();
  }
  
  save() {
    const newImage = Object.assign( Object.create( Object.getPrototypeOf(this.props.image)), this.props.image);
    newImage.name = this.state.imageName;
    ImageController.updateImage(newImage);
    this.hide();
  }
  
  cancel() {
    this.setState({imageName: this.props.image.name});
    this.hide();
  }
  
  render () {
    return (
      <div aria-hidden="true" className="image-modal modal fade show" role="dialog" tabIndex="-1" style={{display: "block"}}>
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-body mb-0 p-0 text-center">
              <img src={this.props.image.highResURL} alt="" style={{maxWidth:"100%", maxHeight: `${window.innerHeight - 100}px`}} />
            </div>
            <div className="modal-footer">
              <input type="text" 
                     className="form-control"
                     aria-label="Search phrase" 
                     aria-describedby="basic-addon2"
                     onChange={this.handleNameChange}
                     value={this.state.imageName} />
              <button className="btn btn-outline-primary btn-rounded btn-md ml-4 text-center" data-dismiss="modal" type="button" onClick={this.delete}>Delete</button>
              <button className="btn btn-outline-primary btn-rounded btn-md ml-4 text-center" data-dismiss="modal" type="button" onClick={this.cancel}>Cancel</button>
              <button className="btn btn-outline-primary btn-rounded btn-md ml-4 text-center" data-dismiss="modal" type="button" onClick={this.save}>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ImageModal.propTypes = {
  image: PropTypes.instanceOf(BaseImage).isRequired,
  onClose: PropTypes.func
};

export default ImageModal;
