import React, { Component } from "react";
import PropTypes from "Utils/propTypes";
import BaseImage from 'Entities/images/BaseImage';
import 'Styles/components/imagesGrid.scss';
import ImageThumbnail from "Components/ImageThumbnail";
import ArrayMethods from 'Utils/ArrayMethods';

class ImagesGrid extends Component {
  constructor (props) {
    super(props);
  }
  
  getImagesForArray(array) {
    const elements = [];
    
    for (let i = 0; i < array.length; i++) {
      const image = array[i]; 
      if (image)
        elements.push(<ImageThumbnail key={image.thumbnailURL} image={image}/>);
    }
    
    return elements;
  }
  
  getImageElements() {
    const totalColumns = 4;
    const imageColumns = ArrayMethods.spreadArrayToColumns(this.props.images, totalColumns); // We use this method to spread the array of images to columns while keeping the order
    
    const elements = [];
    
    for (let i = 0; i < imageColumns.length; i++) {
      elements.push(
        <div className="column" key={`column-${i}`}>
          {this.getImagesForArray(imageColumns[i])}
        </div>
      );
    }
    
    return elements;
  }
  
  render () {
    return (
      <div className="images-grid">
        <div className="text-center">{`Total ${this.props.images.length} images`}</div>
        <div className="row">
          {this.getImageElements()}
        </div>
      </div>
    );
  }
}

ImagesGrid.propTypes = {
  images: PropTypes.arrayOfInstances(BaseImage)
};

export default ImagesGrid;
