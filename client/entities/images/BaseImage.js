class BaseImage {
  constructor(data) {
    this.id = data.id;
    
    this.highResURL = data.highResURL;
    this.width = data.width;
    this.height = data.height;
    
    this.thumbnailURL = data.thumbnailURL;
    this.thumbnailHeight = data.thumbnailHeight;
    this.thumbnailWidth = data.thumbnailWidth;
    
    this.name = data.name;
  }

}

export default BaseImage;
