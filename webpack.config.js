//const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path'),
      webpack = require('webpack'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      { CaseSensitivePathsPlugin } = require('./plugins'),
      CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  target: 'web',
  resolve: {
    alias: {
      Scripts: path.resolve(__dirname, 'client/assets/scripts'),
      Styles: path.resolve(__dirname, 'client/assets/styles'),
      Components: path.resolve(__dirname, 'client/components'),
      Utils: path.resolve(__dirname, 'client/utils'),
      API: path.resolve(__dirname, 'client/api'),
      Entities: path.resolve(__dirname, 'client/entities'),
      Pages: path.resolve(__dirname, 'client/pages'),
      Controllers: path.resolve(__dirname, 'client/controllers'),
    }
  },
  mode: 'development',
  entry: {
    base: [
      'webpack-hot-middleware/client?reload=true'
    ],
    site: [
      path.resolve(__dirname, 'client/index')
    ],
  },
  output: {
    publicPath: '/',
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'client/dist')
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'client')
  },
  context: __dirname,
  module: {
    rules: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {}
    },
    {
      test: /\.(jpe?g|png|gif|ico|svg)$/i,
      loaders: ['file-loader?context=client/assets/images&name=[path][name].[ext]'],
      exclude: /node_modules/,
      include: __dirname,
    },
    {
      test: /\.(jpe?g|png|gif|ico|svg)$/i,
      include: /node_modules/,
      use: [
        'file-loader',
        {
          loader: 'img-loader',
          options: {
            mozjpeg: {
              progressive: true,
              arithmetic: false
            }
          }
        },
      ],
    },
    {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
    {test: /\.(woff|woff2)$/, loader: 'url-loader?prefix=font/&limit=5000'},
    {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
    {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url-loader?limit=10000&mimetype=image/svg+xml',
      include: /node_modules/
    },
    {
      test: /(\.css|\.scss)$/,
      use: [{
        loader: 'style-loader', // inject CSS to page
      },
      {
        loader: 'css-loader', // translates CSS into CommonJS modules
      },
      {
        loader: 'postcss-loader', // Run post css actions
        options: {
          plugins: function () { // post css plugins, can be exported to postcss.config.js
            return [
              require('precss'),
              require('autoprefixer')
            ];
          }
        }
      },
      {
        loader: 'sass-loader' // compiles Sass to CSS
      }]
    }]
  },
  plugins: [
    new CaseSensitivePathsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([{ from: './client/favicon.ico' }]),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new ExtractTextPlugin("[name].bundle.css")
  ]
};
